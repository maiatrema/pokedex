# pokedex

# Pokémon Type Effectiveness

## Type Effectiveness Table

| Type      | Strong Against                     | Weak Against                       |
|-----------|------------------------------------|------------------------------------|
| Normal    | None                               | Fighting                           |
| Fire      | Grass, Bug, Ice, Steel             | Water, Rock, Fire                  |
| Water     | Fire, Ground, Rock                 | Grass, Electric                    |
| Grass     | Water, Ground, Rock                | Fire, Ice, Poison, Flying, Bug     |
| Electric  | Water, Flying                      | Ground                             |
| Ice       | Grass, Ground, Flying, Dragon      | Fire, Fighting, Rock, Steel        |
| Fighting  | Normal, Ice, Rock, Dark, Steel     | Flying, Psychic, Fairy             |
| Poison    | Grass, Fairy                       | Ground, Psychic                    |
| Ground    | Fire, Electric, Poison, Rock, Steel| Water, Grass, Ice                  |
| Flying    | Grass, Fighting, Bug               | Electric, Rock, Ice                |
| Psychic   | Fighting, Poison                   | Bug, Ghost, Dark                   |
| Bug       | Grass, Psychic, Dark               | Fire, Flying, Rock                 |
| Rock      | Fire, Ice, Flying, Bug             | Water, Grass, Fighting, Ground, Steel|
| Ghost     | Psychic, Ghost                     | Ghost, Dark                        |
| Dragon    | Dragon                             | Ice, Dragon, Fairy                 |
| Dark      | Psychic, Ghost                     | Fighting, Bug, Fairy               |
| Steel     | Ice, Rock, Fairy                   | Fire, Fighting, Ground             |
| Fairy     | Fighting, Dragon, Dark             | Poison, Steel                      |





### Normal
```mermaid
graph TD
    Normal -->|weak against| Fighting
```

### Fire

```mermaid
graph TD
    Fire -->|strong against| Grass
    Fire -->|strong against| Bug
    Fire -->|strong against| Ice
    Fire -->|strong against| Steel
    Fire -->|weak against| Water
    Fire -->|weak against| Rock
    Fire -->|weak against| Fire
```

### Water

```mermaid
graph TD
    Water -->|strong against| Fire
    Water -->|strong against| Ground
    Water -->|strong against| Rock
    Water -->|weak against| Grass
    Water -->|weak against| Electric

```

### Grass

```mermaid
graph TD
    Grass -->|strong against| Water
    Grass -->|strong against| Ground
    Grass -->|strong against| Rock
    Grass -->|weak against| Fire
    Grass -->|weak against| Ice
    Grass -->|weak against| Poison
    Grass -->|weak against| Flying
    Grass -->|weak against| Bug
```





