# Guide Pokemon (Red Blue and Yellow)

## Boulder Badge

### Pallet Town

![](../medias/Pokem_2.jpg)

The game begins in your home in Pallet Town. Apart from an exit to the north and three houses, there's not much going on in this sleepy little village.
Your House

After you have said your good-byes to your Mom and familiarized yourself with the controls, it's time to start your adventure. But before you leave, be sure to check your PC and withdraw the stored Potion. There's not really anything else to do here, but your mother will heal your Pokémon for free whenever you stop by later in the game.
Gary's House

Gary, as he is called in the TV show (Blue in the English games and Green in the Japanese Games), is your #1 rival in the game. Gary's house is not only right next to your own, it also looks exactly like it. A sort of doppelganger of yours who will pop up throughout the adventure and loudly proclaim his superior Pokémon training skills, he is more bark than bite -- as you will find out in a few minutes. First, go inside his house and you will meet Gary's sister. She isn't very helpful right now, but be sure to come back later after you received your Pokédex to get the Town Map from her.

Professor Oak's Lab

Don't bother going inside. Professor Oak won't be there. Simply try to leave Pallet Town and he will come running and prompt you to visit his lab.
Red and Blue

Inside, you'll get your very first Pokémon. In Red and Blue, players get to choose their lead Pokémon:

* Bulbasaur
* Squirtle
* Charmander

Which one should you choose? Well, if you want more of a challenge, go with Charmander -- as Bulbasaur and Squirtle have it a little easier early on. My vote goes for Squirtle -- but before you make your choice, click on all three names and read up on their advantages and disadvantages.
Yellow

Right before you get your hands on the Poké Ball, your rival will appear and claim it for himself. Inside, he finds Eevee, one of the game's rarest creatures. Don't worry, not all is lost. Professor Oak has another Pokémon for you: It's everyone's favorite electric mouse, Pikachu.
Battle with Gary
Red and Blue

As you're about to leave, you'll have your first encounter with Gary -- a pretty easy battle that you should have no problem winning. Gary will have chosen a Pokémon that has an advantage over yours, but he wastes a lot of time on status-changing attacks instead of trying to deplete your HP.
Yellow

As you're about to leave, you'll have your first encounter with Gary. It's Pikachu vs. Gary -- a pretty easy battle that you should have no problem winning. The outcome of the battle actually determines what Pokémon Gary will own further on in the quest. If you lose, Gary will later on evolve his Eevee into a Vaporeon. If you win, he may evolve it into either Jolteon or Flareon (which one, in turn depends on the outcome of a battle -- this time, the one that takes place just outside Viridian City).

Once you have defeated Gary, you continue on north. You can heal up your Pikachu by going home and talking to your mom.

#### Later

* Deliver a parcel to Professor Oak later in the game and receive your Pokédex and the ability to use Poké Balls. You will find the parcel in Viridian City's Poké Mart.
* Visit Gary's sister after you received the Pokédex to get the Town Map. 
* Visit Pallet Town when you have the HM 03 - Surf. You will be able to go south.

### Route 1

![](../medias/Pokem_3.jpg)

You can access Route 1 directly from the north of Pallet Town. Since you don't own any Poké Balls yet, there's not much you can do here except for battling Rattata and Pidgey. Note that you only get attacked when you walk through the tall grass. So if you're low on HP and you're trying to avoid fights, stay on the path. Remember to always talk to the people you meet -- otherwise you could miss out on valuable items (such as the Potion you get from the first guy you bump into).

This is also the first time you see the small ledges that run horizontally through the landscape. You can only jump down these ledges from the top to the bottom. They will block your way when you're trying to go the other way. 

### Viridian City

![](../medias/Pokem_4.jpg)

When you move north from Route 1, you will arrive in Viridian City. Here, you will find five buildings and an important item to move along your quest. 


#### Poké Center

![](../medias/Pokem_5.jpg)

Not only can you heal all your Pokémon for free in these handy Poké-hospitals, you can also deposit and withdraw items via the PC system in the corner. If you have a friend who owns a Game Boy and a copy of Pokémon (you'll need a link cable to connect, of course), you can also trade or engage in battle by using the Pokémon Cable Club in any Poké Center in the game. Whenever you see a Poké Center, you should go inside, heal your Pokémon and save -- just in case there are any events that force you to battle. 

#### Poké Mart

You'll find plenty of these stores throughout the whole game. Here, you can buy items ranging from health potions to valuable power-ups. However, the first time you step into this store, you won't be able to buy anything yet. Instead, the store manager asks you to bring a parcel to Professor Oak. Once you have completed this side quest, you can return and buy Potions, Poké Balls and some Antidotes to help you on your further adventures. 

|item| price
| - | -
|PokéBall| 200 
Antidote | 100
Paralyze Heal | 200
Burn Heal | 250 

#### Poké School

Inside this school building, you will find some helpful tips on the game. Just walk up to the blackboard and push your button to read. 

#### Gym

Forget about the Viridian City Gym for now. You'll have to come back here much, much later.
Bushes

There are two small bushes in this city that can be cut down with the help of HM 01 - Cut (which you will find later). Even if you don't have Cut, check the bush in the north (by the old man). You'll find a hidden Potion. Once you cut down the bush in the southwest, you'll get your hands on TM 42 - Dream Eater.

#### Deliver the Parcel

Sorry, you can't move on from here until you have gone back to Pallet Town and delivered the parcel you got in the mart to Professor Oak. In return, you will receive your Pokédex and the ability to use Poké Balls to capture Pokémon. While in Pallet Town, be sure to drop by Gary's sister to get your Town Map.

Once you have delivered the parcel, return to Viridian City and prepare to continue north. If you want, you can also pay a quick visit to Route 22 to the west of this town before you move on. You won't get far, but you'll find Spearow, Nidoran, and Mankey (Yellow). Be sure to take a look at our Game Basics section to learn how to capture and level up Pokémon more effectively.

#### Optional: Battle with Gary

If you've got a few decent Pokémon in your possession, you can challenge Gary to another battle in Route 22. It's a good way to get some experience points and money, but it's not a required event.

#### Red and Blue

Gary will have a Level 9 Pidgey and the same starter Pokemon at Level 8.


#### Yellow

If you've got a few decent Pokémon in your possession, you can challenge Gary to another battle in Route 22. It's a good way to get some experience points and money, but it's not a required event. Note that if you do battle him and you win, Gary will evolve his Eevee into a Jolteon later in the game. If you lose or skip the battle, Gary will evolve his Eevee into a Flareon. Gary owns a Level 9 Spearow, as well as a Level 8 Eevee.

#### Later

* After you have delivered the parcel to Professor Oak, you will be able to go north.
* Once you have learned HM 01 - Cut, you can cut down the bush near the small pond. Talk to the guy behind it and you will get the TM 42 - Dream Eater technique.
* Access the Gym.

### Route 2

![](../medias/Pokem_27.jpg)

Until you have found HM 01 - Cut, Route 2 is merely a short connector that leads to Viridian Forest. If you want, you can also run in circles in that small patch of tall grass to catch Nidoran, Pidgey or Rattata.

If you don't have Cut, move on into the building to the north and jump directly to the Viridian Forest section. 

![](../medias/Pokem_28.jpg)

#### Later:

If you've got Cut, cut down one of the bushes to the right and collect the items behind the trees. You'll find a valuable HP Up and a Moon Stone.

Continuing on the same path, collect HM 05 - Flash inside the house to the east of Viridian Forest. You will only get it if you have collected more than 10 different types of Pokémon.

North of this house (and south of Diglett's Cave, which you can enter from Route 11), you find a small hamlet with an eager trader. If you own a Clefairy, you can get your hands on the elusive Mr. Mime.


### Viridian Forest

![](../medias/pokemon_blue-1-viridianforest.drawio.png)

Welcome to the Viridian City nature preserve. While it may look complicated at first, this forest maze is actually really straightforward. You will encounter plenty of Weedle, and Caterpie with a few Pikachu here and there in Red and Blue. Snag a Pikachu while you're exploring and don't forget to catch a Caterpie, which will evolve into a Metapod at level 7. It's better to catch a young Caterpie instead of its evolved form, Metapod. This will ensure that your Metapod can at least use a basic attack and doesn't have to sit around "hardening" all the time.

You will encounter only Caterpie, Metapod, Pidgey and Pidgeotto in Pokémon Yellow 

#### Trainers

There are a couple of trainers in this forest that will attack you when you get into their line of sight. Remember that you can't run from fights with trainers, nor can you capture their Pokémon.

##### Red and Blue

* Bug Catcher: Weedle (6) Caterpie (6)
* Bug Catcher: Weedle (7), Kakuna (7), Weedle (7)
* Bug Catcher: Weedle (9)

##### Yellow

* Lass: Nidoran Female (6), Nidoran Male (6)
* Bug Catcher: Caterpie (7), Caterpie (7)
* Bug Catcher: Metapod (6), Metapod (6), Caterpie (6)
* Bug Catcher: Caterpie (8), Metapod (8)
* Bug Catcher: Caterpie (10)

#### Through the Forest

Make a left when you first enter the forest. Now continue on north and turn left again to get your hands on a Poké Ball.
Make your way east, pass the area where you first entered and go up north. You will encounter bug catchers on your way. Be sure to stick to the left after the second one, as you will find an Antidote on the path.
Don't bother going right. There's nothing there. Instead, make a left into the grassy area and continue down south.
Follow the path north, then south through two grassy areas. Once you reach the sign, go east and down to find a Potion.
You're almost done. Go west to the very left and head up north. That's it. The exit is straight ahead.

### Pewter City

![](../medias/Pokem_29.jpg)

You have arrived in Pewter City, home of a famous Science Museum and your very first Gym challenge. You can explore the town at your heart's content, but you can't leave before you face off with Pewter City's Gym Leader, Brock.

If you're low on HP or items, be sure to visit the Pokémon Mart and a Pokémon Center here.

#### Pokémon Mart

item | price
| - | - |
PokéBall | 200
Potion 	| 300
Escape Rope | 550
Antidote | 100
Burn Heal | 250
Awakening | 200
Paralyze Heal | 200 

#### Science Museum

The biggest building, up north. It costs 50 bucks to get in -- but there isn't really anything you can do here until later.

Still, you might want to take a look at the exhibit just to ease your curiosity.

The real reason to visit here is actually in the back. Once you have gained the ability to Cut, you can take down the bush outside the museum and enter through a side door. Inside, you will receive the Old Amber, so be sure to come back here once you get Cut.
Pewter City Gym

Every major city in the Pokémon universe has a Pokémon Gym, established by the official association of Pokémon trainers. Inside these gyms, you will face one or more Pokémon trainers headed up by a strong leader. The first gym leader you will face, Brock, isn't all that tough to beat -- unless you mismatch types. Yellow players actually have more of a struggle here (Pikachu is inherently weak against Ground/Rock types, which are of course Brock's preferred Pokémon). Water Pokémon like Squirtle won't have any problems blazing through this battle. If you don't have Squirtle, use a Fighting Pokémon or a Level 12 (or higher) Normal type like Nidoran for an easy battle. Don't use Poison or Electric techniques, though.

The trainers appearing in this gym are:

#### Red and Blue

* Jr. Trainer Male: Diglett (11), Sandshrew (11)
* Brock: Geodude (12), Onix (14)

#### Yellow

* Jr. Trainer Male: Diglett (9), Sandshrew (9)
* Brock: Geodude (10), Onix (12) 

Once you beat Brock, you get your hands on TM 34 (Bide) and the Boulder Badge. Apart from allowing you to go on, the Boulder Badge also gives you the ability to utilize the Flash technique to light up dark dungeons. 

Note that you need to first find HM 05 - Flash in order to actually learn this technique. But in order to get Flash, you first need to learn Cut. So forget about it for now and move on to Route 3, to the east. 

#### Later: 

* Once you've got Cut, cut down the bush on the east side of the museum. You can enter through a side door where you will receive the Old Amber. 

## Cascade Badge

### Route 3

Route 3 leads directly to Mt. Moon. But in order to pass, you need to do battle with a number of Pokémon trainers. 

#### Trainers

If you don't want to fight, it's pretty easy to avoid them by staying out of their line of sight. But remember that you need to build up experience and level up your Pokémon. Avoiding battles can be a really bad idea.

* Lass: Pidgey (9), Pidgey (9)
* Bug Catcher: Caterpie (10), Caterpie (10), Weedle (10)
* Youngster: Rattata (11), Ekans (11)
* Bug Catcher: Weedle (9), Kakuna (9), Caterpie (9), Metapod (9)
* Lass: Rattata (10), Nidoran Male (10)
* Youngster: Spearow (14)
* Bug Catcher: Caterpie (11), Metapod (11)
* Lass: Jigglypuff (14)

#### Mt. Moon Poké Center

![](../medias/Pokem_44.jpg)


You'll find this lone center in the northeast of Route 3. It's a good idea to heal your Pokémon here and save your game as you're about to enter Mt. Moon. You can also buy a Magikarp from a travelling salesman for 500. Do you have a lot of cash?

Well, if you do, then go for it. Magikarp is a really common Pokémon and you'll get the chance to catch tons of them later -- but if you grab one now and stick it at the top of your party, you'll soon be able to evolve it into the powerful Gyarados. And yes, you do want one. And if you get the Magikarp do not give it to the daycare for too long... Cause if you do that Magikarp will not evolve at all

That's all there is to do here. Now enter Mt. Moon through the tunnel entrance

### Mt. Moon



### Cerulean City

## Thunder Badge

### Route 24
### Route 25
### Back to Cerulean
### Route 5
### Route 6
### Vermilion City
### S.S. Anne
### Vermilion City Gym

## Rainbow Badge

### Route 11
### Diglett's Cave
### Route 2 Return
### Pewter City Return
### Cerulean City Return
### Route 9
### Rock Tunnel
### Route 10
### Lavender Town
### Route 8
### Route 7
### Celadon City

## Marsh Badge

### Pokemon Tower
### Saffron City

## Soul Badge
 
### Route 12
### Route 13
### Route 14
### Route 15
### Fuchsia City

## Volcano Badge

### Route 18
### Route 17
### Route 16
### Power Plant
### Route 19
### Seafoam Islands
### Route 20
### Cinnabar Island

## Earth Badge

### Route 21
### Viridian City Gym
### Victory Road
### Indigo Plateau

## Post Game

### Unknown Dungeon

