guide pokemon

# Guide Pokemon (rouge bleu et jaune)

## Boulder Badge

### Pallet Town
### Route 1
### Viridian City
### Route 2
### Viridian Forest
### Pewter City

## Cascade Badge

### Route 3
### Mt. Moon
### Cerulean City

## Thunder Badge

### Route 24
### Route 25
### Back to Cerulean
### Route 5
### Route 6
### Vermilion City
### S.S. Anne
### Vermilion City Gym

## Rainbow Badge

### Route 11
### Diglett's Cave
### Route 2 Return
### Pewter City Return
### Cerulean City Return
### Route 9
### Rock Tunnel
### Route 10
### Lavender Town
### Route 8
### Route 7
### Celadon City

## Marsh Badge

### Pokemon Tower
### Saffron City

## Soul Badge
 
### Route 12
### Route 13
### Route 14
### Route 15
### Fuchsia City

## Volcano Badge

### Route 18
### Route 17
### Route 16
### Power Plant
### Route 19
### Seafoam Islands
### Route 20
### Cinnabar Island

## Earth Badge

### Route 21
### Viridian City Gym
### Victory Road
### Indigo Plateau

## Post Game

### Unknown Dungeon

